# Script to perform hyperparameter tuning for overall and per domain classifier

import subprocess
import itertools
import os

from gpu_executor import GPUExecutor

# settings

dataset_path = "data/dataset_v2"

# config file
config_file = f'allennlp_config/ner_hyperparam.json'

# bert files
is_lowercase = 'true'
bert_vocab = 'bert_model/scibert_scivocab_uncased/vocab.txt'
bert_weights = 'bert_model/scibert_scivocab_uncased/weights.tar.gz'

folds = [0, 1, 2, 3, 4]

# ADAPT: provide the GPUs to be used. For each GPU a concurrent training is executed.
#gpus=[0, 1, 2, 3, 4, 5, 6, 7]
gpus=[0]

# ADAPT: provide the prefix for the runs. The result files will be stored in the folder
#   "results/{run_prefix}_{domain}/{combinations}"
run_prefix = "stm_run_2019-05-08_stm_v2"

hyper_params = {
    "overall": {
        "dropouts": [0.2, 0.5],
        "lstm_hss": [200, 350, 500, 768],
        "lrs": [0.005, 0.001],
    },
    "Agriculture": {
        "dropouts": [0.2, 0.5],
        "lstm_hss": [200, 350, 500, 768],
        "lrs": [0.005, 0.001],
    },
    "Astronomy": {
        "dropouts": [0.2, 0.5],
        "lstm_hss": [200, 350, 500, 768],
        "lrs": [0.005, 0.001],
    },
    "Biology": {
        "dropouts": [0.2, 0.5],
        "lstm_hss": [200, 350, 500, 768],
        "lrs": [0.005, 0.001],
    },
    "Chemistry": {
        "dropouts": [0.2, 0.5],
        "lstm_hss": [200, 350, 500, 768],
        "lrs": [0.005, 0.001],
    },
    "Computer_Science": {
        "dropouts": [0.2, 0.5],
        "lstm_hss": [200, 350, 500, 768],
        "lrs": [0.005, 0.001],
    },
    "Earth_Science": {
        "dropouts": [0.2, 0.5],
        "lstm_hss": [200, 350, 500, 768],
        "lrs": [0.005, 0.001],
    },
    "Engineering": {
        "dropouts": [0.2, 0.5],
        "lstm_hss": [200, 350, 500, 768],
        "lrs": [0.005, 0.001],
    },
    "Materials_Science": {
        "dropouts": [0.2, 0.5],
        "lstm_hss": [200, 350, 500, 768],
        "lrs": [0.005, 0.001],
    },

    "Mathematics": {
        "dropouts": [0.2, 0.5],
        "lstm_hss": [200, 350, 500, 768],
        "lrs": [0.005, 0.001],
    },
    "Medicine": {
        "dropouts": [0.2, 0.5],
        "lstm_hss": [200, 350, 500, 768],
        "lrs": [0.005, 0.001],
    }
}


##################################

seed = 13270
pytorch_seed = seed // 10
numpy_seed = pytorch_seed // 10


def train_func(conf_path_with_params, output_path, dropout, lstm_hs, lr, train_path, dev_path, test_path):
    def f(gpu):
        cmd = ' '.join(['python',
                        '-m',
                        'allennlp.run',
                        'train',
                        f'{conf_path_with_params}',
                        '-s',
                        output_path])
        env = {
            'DROPOUT': str(dropout),
            'LSTM_HS': str(lstm_hs),
            'LR': str(lr),
            'BERT_VOCAB': bert_vocab,
            'BERT_WEIGHTS': bert_weights,
            'TRAIN_PATH': train_path,
            'DEV_PATH': dev_path,
            'TEST_PATH': test_path,
            'IS_LOWERCASE': str(is_lowercase),
            'SEED': str(seed),
            'PYTORCH_SEED': str(pytorch_seed),
            'NUMPY_SEED': str(numpy_seed),
            'CUDA_VISIBLE_DEVICES': str(gpu)
        }
        print(env)
        print(cmd)
        completed = subprocess.run(cmd, shell=True, env=env)
        print(f'returncode: {completed.returncode}')

    return f


def train_per_domain():
    for domain in hyper_params:
        executor = GPUExecutor(gpus=gpus)

        dropouts = hyper_params[domain]["dropouts"]
        lstm_hss = hyper_params[domain]["lstm_hss"]
        lrs = hyper_params[domain]["lrs"]

        run = f"{run_prefix}_{domain}"

        with open(config_file, "r", encoding="utf-8") as hp:
            hyper_params_template = hp.read()

        run_path = f'results/{run}'

        templates_tmp_path = f"{run_path}/templates_tmp"
        os.makedirs(templates_tmp_path, exist_ok=True)

        for (fold, dropout, lstm_hs, lr) in list(itertools.product(*[folds, dropouts, lstm_hss, lrs])):
            combination = f'stm_fold_{fold}_dr_{dropout}_lstm_hs_{lstm_hs}_lr_{lr}'
            output_path = f'{run_path}/{combination}'
            train_path = f'{dataset_path}/folds/fold_{fold}/conll/train/{domain}/train.txt'
            dev_path = f'{dataset_path}/folds/fold_{fold}/conll/dev/{domain}/dev.txt'
            test_path = f'{dataset_path}/folds/fold_{fold}/conll/test/{domain}/test.txt'

            # jsonnet does not support parsing floats from env vars, therefore this hack... :-/
            conf_path_with_params = os.path.join(templates_tmp_path, f"config_tpl_{combination}.tmp")
            with open(conf_path_with_params, "w", encoding="utf-8") as hpf:
                hps = hyper_params_template.replace("$DROPOUT", str(dropout))
                hps = hps.replace("$LR", str(lr))
                hps = hps.replace("$LSTM_HS", str(lstm_hs))
                hps = hps.replace("$GPU", "0")
                hpf.write(hps)

            executor.submit(train_func(
                conf_path_with_params, output_path, dropout, lstm_hs, lr, train_path, dev_path, test_path))

        executor.shutdown()

train_per_domain()
