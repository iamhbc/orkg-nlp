-DOCSTART- (S0167278913001450)

Finite	0,6	O	B-Data
-	6,7	O	I-Data
time	7,11	O	I-Data
Lyapunov	12,20	O	I-Data
exponents	21,30	O	I-Data
(	31,32	O	I-Data
FTLE	32,36	O	I-Data
)	36,37	O	I-Data
are	38,41	O	O
often	42,47	O	O
used	48,52	O	O
to	53,55	O	O
identify	56,64	O	O
Lagrangian	65,75	O	B-Data
Coherent	76,84	O	I-Data
Structures	85,95	O	I-Data
(	96,97	O	I-Data
LCS	97,100	O	I-Data
)	100,101	O	I-Data
.	101,102	O	O

Most	103,107	O	O
applications	108,120	O	O
are	121,124	O	O
confined	125,133	O	O
to	134,136	O	O
flows	137,142	O	B-Process
on	143,145	O	O
two	146,149	O	B-Material
-	149,150	O	I-Material
dimensional	150,161	O	I-Material
(	162,163	O	I-Material
2D	163,165	O	I-Material
)	165,166	O	I-Material
surfaces	167,175	O	I-Material
where	176,181	O	O
the	182,185	O	B-Data
LCS	186,189	O	I-Data
are	190,193	O	O
characterized	194,207	O	O
as	208,210	O	O
curves	211,217	O	B-Data
.	217,218	O	O

The	219,222	O	O
extension	223,232	O	O
to	233,235	O	O
three	236,241	O	B-Process
-	241,242	O	I-Process
dimensional	242,253	O	I-Process
(	254,255	O	I-Process
3D	255,257	O	I-Process
)	257,258	O	I-Process
flows	259,264	O	I-Process
,	264,265	O	O
whose	266,271	O	O
LCS	272,275	O	B-Data
are	276,279	O	O
2D	280,282	O	B-Material
structures	283,293	O	I-Material
embedded	294,302	O	B-Process
in	303,305	O	O
a	306,307	O	B-Material
3D	308,310	O	I-Material
volume	311,317	O	I-Material
,	317,318	O	O
is	319,321	O	O
theoretically	322,335	O	O
straightforward	336,351	O	O
.	351,352	O	O

However	353,360	O	O
,	360,361	O	O
in	362,364	O	O
geophysical	365,376	O	B-Process
flows	377,382	O	I-Process
at	383,385	O	O
regional	386,394	O	B-Data
scales	395,401	O	I-Data
,	401,402	O	O
full	403,407	O	B-Process
prognostic	408,418	O	I-Process
computation	419,430	O	I-Process
of	431,433	O	O
the	434,437	O	B-Material
evolving	438,446	O	I-Material
3D	447,449	O	I-Material
velocity	450,458	O	I-Material
field	459,464	O	I-Material
is	465,467	O	O
not	468,471	O	O
computationally	472,487	O	O
feasible	488,496	O	O
.	496,497	O	O

The	498,501	O	B-Data
vertical	502,510	O	I-Data
or	511,513	O	I-Data
diabatic	514,522	O	I-Data
velocity	523,531	O	I-Data
,	531,532	O	O
then	533,537	O	O
,	537,538	O	O
is	539,541	O	O
either	542,548	O	O
ignored	549,556	O	B-Process
or	557,559	O	I-Process
estimated	560,569	O	I-Process
as	570,572	O	O
a	573,574	O	B-Data
diagnostic	575,585	O	I-Data
quantity	586,594	O	I-Data
with	595,599	O	O
questionable	600,612	O	B-Data
accuracy	613,621	O	I-Data
.	621,622	O	O

Even	623,627	O	O
in	628,630	O	O
cases	631,636	O	O
with	637,641	O	O
reliable	642,650	O	B-Data
3D	651,653	O	I-Data
velocities	654,664	O	I-Data
,	664,665	O	O
it	666,668	O	O
may	669,672	O	O
prove	673,678	O	O
advantageous	679,691	O	O
to	692,694	O	O
minimize	695,703	O	O
the	704,707	O	O
computational	708,721	O	O
burden	722,728	O	O
by	729,731	O	O
calculating	732,743	O	B-Process
trajectories	744,756	O	B-Data
from	757,761	O	O
velocities	762,772	O	B-Data
on	773,775	O	O
carefully	776,785	O	B-Material
chosen	786,792	O	I-Material
surfaces	793,801	O	I-Material
only	802,806	O	O
.	806,807	O	O

When	808,812	O	O
reliable	813,821	O	B-Data
3D	822,824	O	I-Data
velocity	825,833	O	I-Data
information	834,845	O	I-Data
is	846,848	O	O
unavailable	849,860	O	O
or	861,863	O	O
one	864,867	O	B-Data
velocity	868,876	O	I-Data
component	877,886	O	I-Data
is	887,889	O	O
explicitly	890,900	O	O
ignored	901,908	O	O
,	908,909	O	O
a	910,911	O	B-Data
reduced	912,919	O	I-Data
FTLE	920,924	O	I-Data
form	925,929	O	I-Data
to	930,932	O	O
approximate	933,944	O	O
2D	945,947	O	B-Material
LCS	948,951	O	I-Material
surfaces	952,960	O	I-Material
in	961,963	O	O
a	964,965	O	B-Material
3D	966,968	O	I-Material
volume	969,975	O	I-Material
is	976,978	O	O
necessary	979,988	O	O
.	988,989	O	O

The	990,993	O	B-Data
accuracy	994,1002	O	I-Data
of	1003,1005	O	O
two	1006,1009	O	B-Data
reduced	1010,1017	O	B-Data
FTLE	1018,1022	O	I-Data
formulations	1023,1035	O	I-Data
is	1036,1038	O	O
assessed	1039,1047	O	O
here	1048,1052	O	O
using	1053,1058	O	O
the	1059,1062	O	B-Process
ABC	1063,1066	O	I-Process
flow	1067,1071	O	I-Process
and	1072,1075	O	O
a	1076,1077	O	B-Process
3D	1078,1080	O	I-Process
quadrupole	1081,1091	O	I-Process
flow	1092,1096	O	I-Process
as	1097,1099	O	O
test	1100,1104	O	B-Data
models	1105,1111	O	I-Data
.	1111,1112	O	O

One	1113,1116	O	O
is	1117,1119	O	O
the	1120,1123	O	O
standard	1124,1132	O	O
approach	1133,1141	O	O
of	1142,1144	O	O
knitting	1145,1153	O	B-Process
together	1154,1162	O	I-Process
FTLE	1163,1167	O	B-Data
patterns	1168,1176	O	I-Data
obtained	1177,1185	O	O
on	1186,1188	O	O
adjacent	1189,1197	O	B-Material
surfaces	1198,1206	O	I-Material
.	1206,1207	O	O

The	1208,1211	O	O
other	1212,1217	O	O
is	1218,1220	O	O
a	1221,1222	O	B-Material
new	1223,1226	O	I-Material
approximation	1227,1240	O	I-Material
accounting	1241,1251	O	O
for	1252,1255	O	O
the	1256,1259	O	B-Process
dispersion	1260,1270	O	I-Process
due	1271,1274	O	O
to	1275,1277	O	O
vertical	1278,1286	O	B-Process
(	1287,1288	O	I-Process
u	1288,1289	O	I-Process
,	1289,1290	O	I-Process
v	1290,1291	O	I-Process
)	1291,1292	O	I-Process
shear	1293,1298	O	I-Process
.	1298,1299	O	O

The	1300,1303	O	O
results	1304,1311	O	O
are	1312,1315	O	O
compared	1316,1324	O	O
with	1325,1329	O	O
those	1330,1335	O	O
obtained	1336,1344	O	O
from	1345,1349	O	O
the	1350,1353	O	B-Material
full	1354,1358	O	I-Material
3D	1359,1361	O	I-Material
velocity	1362,1370	O	I-Material
field	1371,1376	O	I-Material
.	1376,1377	O	O

We	1378,1380	O	O
introduce	1381,1390	O	O
two	1391,1394	O	B-Process
diagnostic	1395,1405	O	I-Process
quantities	1406,1416	O	I-Process
to	1417,1419	O	O
identify	1420,1428	O	O
situations	1429,1439	O	O
when	1440,1444	O	O
a	1445,1446	O	B-Method
fully	1447,1452	O	I-Method
3D	1453,1455	O	I-Method
computation	1456,1467	O	I-Method
is	1468,1470	O	O
required	1471,1479	O	O
for	1480,1483	O	O
an	1484,1486	O	B-Process
accurate	1487,1495	O	I-Process
determination	1496,1509	O	I-Process
of	1510,1512	O	O
the	1513,1516	O	B-Data
2D	1517,1519	O	I-Data
LCS	1520,1523	O	I-Data
.	1523,1524	O	O

For	1525,1528	O	O
the	1529,1532	O	B-Process
ABC	1533,1536	O	I-Process
flow	1537,1541	O	I-Process
,	1541,1542	O	O
we	1543,1545	O	O
found	1546,1551	O	O
the	1552,1555	O	B-Data
full	1556,1560	O	I-Data
3D	1561,1563	O	I-Data
calculation	1564,1575	O	I-Data
to	1576,1578	O	O
be	1579,1581	O	O
necessary	1582,1591	O	O
unless	1592,1598	O	O
the	1599,1602	O	B-Data
vertical	1603,1611	O	I-Data
(	1612,1613	O	I-Data
u	1613,1614	O	I-Data
,	1614,1615	O	I-Data
v	1615,1616	O	I-Data
)	1616,1617	O	I-Data
shear	1618,1623	O	I-Data
is	1624,1626	O	O
sufficiently	1627,1639	O	B-Data
small	1640,1645	O	I-Data
.	1645,1646	O	O

However	1647,1654	O	O
,	1654,1655	O	O
both	1656,1660	O	O
methods	1661,1668	O	O
compare	1669,1676	O	O
favorably	1677,1686	O	O
with	1687,1691	O	O
the	1692,1695	O	B-Data
3D	1696,1698	O	I-Data
calculation	1699,1710	O	I-Data
for	1711,1714	O	O
the	1715,1718	O	B-Data
quadrupole	1719,1729	O	I-Data
model	1730,1735	O	I-Data
scaled	1736,1742	O	B-Process
to	1743,1745	O	O
typical	1746,1753	O	B-Data
open	1754,1758	O	I-Data
ocean	1759,1764	O	I-Data
conditions	1765,1775	O	I-Data
.	1775,1776	O	O

Highlights	1777,1787	O	O
•	1788,1789	O	O
Approximations	1790,1804	O	B-Data
of	1805,1807	O	O
2D	1808,1810	O	B-Material
Lagrangian	1811,1821	O	I-Material
coherent	1822,1830	O	I-Material
structures	1831,1841	O	I-Material
in	1842,1844	O	O
3D	1845,1847	O	B-Process
flows	1848,1853	O	I-Process
are	1854,1857	O	O
studied	1858,1865	O	O
.	1865,1866	O	O

•	1867,1868	O	O

Vertical	1869,1877	O	B-Data
shear	1878,1883	O	I-Data
is	1884,1886	O	O
shown	1887,1892	O	O
to	1893,1895	O	O
be	1896,1898	O	O
important	1899,1908	O	O
in	1909,1911	O	O
the	1912,1915	O	B-Process
three	1916,1921	O	I-Process
-	1921,1922	O	I-Process
dimensional	1922,1933	O	I-Process
flows	1934,1939	O	I-Process
.	1939,1940	O	O

•	1941,1942	O	O

A	1943,1944	O	B-Process
new	1945,1948	O	I-Process
approximate	1949,1960	O	I-Process
Cauchy	1961,1967	O	I-Process
-	1967,1968	O	I-Process
Green	1968,1973	O	I-Process
tensor	1974,1980	O	I-Process
without	1981,1988	O	O
using	1989,1994	O	O
vertical	1995,2003	O	B-Data
velocities	2004,2014	O	I-Data
is	2015,2017	O	O
proposed	2018,2026	O	O
.	2026,2027	O	O

•	2028,2029	O	O

Two	2030,2033	O	B-Data
velocity	2034,2042	O	B-Process
gradient	2043,2051	O	I-Process
diagnostics	2052,2063	O	I-Process
help	2064,2068	O	O
to	2069,2071	O	O
decide	2072,2078	O	O
when	2079,2083	O	O
the	2084,2087	O	B-Data
full	2088,2092	O	I-Data
3D	2093,2095	O	I-Data
velocity	2096,2104	O	I-Data
is	2105,2107	O	O
needed	2108,2114	O	O
.	2114,2115	O	O

-DOCSTART- (S0304406813000335)

We	0,2	O	O
introduce	3,12	O	O
efficient	13,22	O	B-Material
sets	23,27	O	I-Material
,	27,28	O	O
a	29,30	O	B-Material
class	31,36	O	I-Material
of	37,39	O	I-Material
sets	40,44	O	I-Material
in	45,47	O	O
Rp	48,50	O	B-Data
in	51,53	O	O
which	54,59	O	O
,	59,60	O	O
in	61,63	O	O
each	64,68	O	B-Material
set	69,72	O	I-Material
,	72,73	O	O
no	74,76	O	O
element	77,84	O	B-Data
is	85,87	O	O
greater	88,95	O	B-Process
in	96,98	O	O
all	99,102	O	B-Data
dimensions	103,113	O	I-Data
than	114,118	O	O
any	119,122	O	O
other	123,128	O	O
.	128,129	O	O

Neither	130,137	O	O
differentiability	138,155	O	B-Process
nor	156,159	O	O
continuity	160,170	O	B-Process
is	171,173	O	O
required	174,182	O	O
of	183,185	O	O
such	186,190	O	B-Material
sets	191,195	O	I-Material
,	195,196	O	O
which	197,202	O	O
include	203,210	O	O
:	210,211	O	O
level	212,217	O	B-Material
sets	218,222	O	I-Material
of	223,225	O	O
utility	226,233	O	B-Data
functions	234,243	O	I-Data
,	243,244	O	O
quasi	245,250	O	B-Data
-	250,251	O	I-Data
indifference	251,263	O	I-Data
classes	264,271	O	I-Data
associated	272,282	O	O
with	283,287	O	O
a	288,289	O	B-Data
preference	290,300	O	I-Data
relation	301,309	O	I-Data
not	310,313	O	O
given	314,319	O	O
by	320,322	O	O
a	323,324	O	B-Data
utility	325,332	O	I-Data
function	333,341	O	I-Data
,	341,342	O	O
mean	343,347	O	B-Data
-	347,348	O	I-Data
variance	348,356	O	I-Data
frontiers	357,366	O	I-Data
,	366,367	O	O
production	368,378	O	B-Data
possibility	379,390	O	I-Data
frontiers	391,400	O	I-Data
,	400,401	O	O
and	402,405	O	O
Pareto	406,412	O	B-Data
efficient	413,422	O	I-Data
sets	423,427	O	I-Data
.	427,428	O	O

By	429,431	O	O
Lebesgue	432,440	O	B-Method
's	440,442	O	I-Method
density	443,450	O	I-Method
theorem	451,458	O	I-Method
,	458,459	O	O
efficient	460,469	O	B-Material
sets	470,474	O	I-Material
have	475,479	O	O
p	480,481	O	B-Data
-	481,482	O	I-Data
dimensional	482,493	O	I-Data
measure	494,501	O	I-Data
zero	502,506	O	I-Data
.	506,507	O	O

As	508,510	O	O
Lebesgue	511,519	O	B-Data
measure	520,527	O	I-Data
provides	528,536	O	O
an	537,539	O	O
imprecise	540,549	O	O
description	550,561	O	O
of	562,564	O	O
small	565,570	O	B-Material
sets	571,575	O	I-Material
,	575,576	O	O
we	577,579	O	O
then	580,584	O	O
prove	585,590	O	O
the	591,594	O	B-Data
stronger	595,603	O	I-Data
result	604,610	O	I-Data
that	611,615	O	O
each	616,620	O	B-Material
efficient	621,630	O	I-Material
set	631,634	O	I-Material
in	635,637	O	O
Rp	638,640	O	B-Data
has	641,644	O	O
Hausdorff	645,654	O	B-Data
dimension	655,664	O	I-Data
at	665,667	O	I-Data
most	668,672	O	I-Data
p-1	673,676	O	I-Data
.	676,677	O	O

This	678,682	O	O
may	683,686	O	O
exceed	687,693	O	O
its	694,697	O	O
topological	698,709	O	B-Data
dimension	710,719	O	I-Data
,	719,720	O	O
with	721,725	O	O
the	726,729	O	O
two	730,733	O	O
notions	734,741	O	O
becoming	742,750	O	O
equivalent	751,761	O	O
for	762,765	O	O
smooth	766,772	O	B-Material
sets	773,777	O	I-Material
.	777,778	O	O

We	779,781	O	O
apply	782,787	O	O
these	788,793	O	O
results	794,801	O	O
to	802,804	O	O
stable	805,811	O	B-Material
sets	812,816	O	I-Material
in	817,819	O	O
multi	820,825	O	B-Process
-	825,826	O	I-Process
good	826,830	O	I-Process
pillage	831,838	O	I-Process
games	839,844	O	I-Process
:	844,845	O	O
for	846,849	O	O
n	850,851	O	B-Data
agents	852,858	O	I-Data
and	859,862	O	O
m	863,864	O	B-Data
goods	865,870	O	I-Data
,	870,871	O	O
stable	872,878	O	B-Material
sets	879,883	O	I-Material
have	884,888	O	O
dimension	889,898	O	B-Data
at	899,901	O	I-Data
most	902,906	O	I-Data
m(n-1)-1	907,915	O	I-Data
.	915,916	O	O

This	917,921	O	O
implies	922,929	O	O
,	929,930	O	O
and	931,934	O	O
is	935,937	O	O
much	938,942	O	O
stronger	943,951	O	O
than	952,956	O	O
,	956,957	O	O
the	958,961	O	O
result	962,968	O	O
that	969,973	O	O
stable	974,980	O	B-Material
sets	981,985	O	I-Material
have	986,990	O	O
m(n-1)-dimensional	991,1009	O	B-Data
measure	1010,1017	O	I-Data
zero	1018,1022	O	I-Data
,	1022,1023	O	O
as	1024,1026	O	O
conjectured	1027,1038	O	O
by	1039,1041	O	O
Jordan	1042,1048	O	O
.	1048,1049	O	O

