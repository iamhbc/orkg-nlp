"""

Load an Allennlp model and make predictions.  Then format predictions into
Semeval2017 format to use their evaluation script.

Code originally taken from https://github.com/allenai/scibert/blob/kylel/semeval2017/scripts/predict_semeval2017_with_allennlp.py

"""
import argparse

import os
import json

from collections import defaultdict
import time

from allennlp.common import Params
from allennlp.data import DatasetReader
from allennlp.models import Model
import math
from standoffreader import read_conll_allennlp, Document, Token, sentences_to_entities, write_entities, \
    write_to_conll_allennlp


def load_bert_reader_model_experiment_dir(experiment_dir: str,
                                          cuda_device: int = -1):
    # check values of existing config
    config_file = os.path.join(experiment_dir, 'config.json')

    config = Params.from_file(config_file)

    # instantiate dataset reader
    reader = DatasetReader.from_params(config["dataset_reader"])

    # instantiate model w/ pretrained weights
    model = Model.load(config.duplicate(),
                       weights_file=os.path.join(experiment_dir, 'best.th'),
                       serialization_dir=experiment_dir,
                       cuda_device=cuda_device)

    # set training=false for prediction
    model.eval()

    return reader, model


def load_spans_for_tokens(conll_file: str):
    all_tokens_spans = []
    token_gold_labels = []

    docs = read_conll_allennlp(conll_file)
    for d in docs:
        for s in d.sentences:
            token_spans = []
            for t in s:
                token_spans.append({
                    'id': d.basename,
                    'token': t.text,
                    'span': f'{t.start},{t.end}'
                })
                token_gold_labels.append((t.text, t.labels[0]))
            all_tokens_spans.append(token_spans)

    return all_tokens_spans, token_gold_labels



def make_chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i+n]


def split_label(label_txt):
    if label_txt == 'O':
        bio = label_txt
        label = label_txt
    else:
        bio, label = label_txt.split('-')
    return bio, label

def extract_spans_with_allennlp(all_tokens_spans, instances, model, sentence_wise):
    s = time.time()

    predicted_labels = []
    outs = []

    chunks = list(make_chunks(instances, 64))
    print('predicting chunks: ' + str(len(chunks)), flush=True)
    for i, c in enumerate(chunks):
        print("Predicting chunk:" + str(i), flush=True)
        chunk_out = model.forward_on_instances(c)
        outs.extend(chunk_out)
    print('predicting chunks finished', flush=True)

    doc_likelihoods = None
    prev_id = ""

    docs = []
    cdoc = None
    for instance, token_spans, out in zip(instances, all_tokens_spans, outs):
        id = token_spans[0]['id']

        # load all the variables and check both lists align
        assert len(instance['metadata'].metadata['words']) == len(token_spans)

        pred_tags = out['tags']
        tokens = out['words']

        if "log_likelihood" in out:
            if doc_likelihoods is None:
                doc_likelihoods = []

            if sentence_wise or prev_id != id:
                # document changed
                cur_doc = dict()
                cur_doc["id"] = id
                cur_doc["sentences"] = []
                doc_likelihoods.append(cur_doc)

            log_likelihood = out["log_likelihood"]
            sent_info = dict()
            sent_info["n_tokens"] = len(tokens)
            sent_info["log_likelihood"] = log_likelihood
            sent_info["likelihood"] = math.exp(log_likelihood)
            sent_info["mnlp"] = log_likelihood / len(tokens)
            doc_likelihoods[-1]["sentences"].append(sent_info)

        spans = [tuple(int(i) for i in d['span'].split(',')) for d in token_spans]
        assert [d['token'] for d in token_spans] == tokens
        assert len(pred_tags) == len(tokens) == len(spans)

        if prev_id != id:
            cdoc = Document([], None, id)
            docs.append(cdoc)


        sentence = []
        for token, pred_tag, span in zip(tokens, pred_tags, spans):

            bio, label = split_label(pred_tag)
            # map BIOUL to BIO
            if bio == 'U':
                bio = 'B'
            elif bio == 'L':
                bio = 'I'
            l = "O" if label=="O" else f'{bio}-{label}'
            sentence.append(Token(labels=[l], text=token, start=span[0], end=span[1]))

            predicted_labels.append(label)
        prev_id = id
        cdoc.sentences.append(sentence)

    e = time.time()
    print(f'Time took for prediction: {e - s} sec')
    return docs, doc_likelihoods, predicted_labels


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--experiment_dir', type=str,
                        help='Location of experiment result')
    parser.add_argument('--conll_file', type=str,
                        help='CONLL2003 file to predict')
    parser.add_argument('--output_dir', type=str,
                        help='Output directory to dump predicted .ann files')
    parser.add_argument('--gpu', action='store_true',
                        help='Flag to use GPU')
    parser.add_argument('--sentence_wise', action='store_true',
                        help='Flag to write sentence-wise or document wise log likelihoods.')
    args = parser.parse_args()

    print("cuda device", flush=True)
    print(os.environ["CUDA_VISIBLE_DEVICES"])

    reader, model = load_bert_reader_model_experiment_dir(experiment_dir=args.experiment_dir,
                                                          cuda_device=0 if args.gpu else -1)
    # collect token spans from the dataset
    print("loading data ...", flush=True)
    all_tokens_spans, token_gold_labels = load_spans_for_tokens(args.conll_file)

    # read instances for prediction
    print("loading data (allen)...", flush=True)
    instances = reader.read(args.conll_file)

    # remove labels of the instances as sometimes unknown labels may be in the test data
    for ins in instances:
        ins["tags"].labels = ["O"] * len(ins["tags"].labels)

    # predict while aligning read spans with each token
    docs, doc_likelihoods, token_predicted_labels = \
        extract_spans_with_allennlp(all_tokens_spans, instances, model, args.sentence_wise)

    assert len(token_gold_labels) == len(token_predicted_labels)

    # write in `.ann` format to match with semeval
    os.makedirs(args.output_dir, exist_ok=True)
    for d in docs:
        with open(os.path.join(args.output_dir, f'{d.basename}.ann'), 'w', encoding="utf-8") as f_out:
            entities = sentences_to_entities(orig_text=None, sentences=d.sentences)
            entities_text = write_entities(entities)
            f_out.write(entities_text)

    #write conll file
    with open(os.path.join(args.output_dir, 'predicted.txt'), 'w', encoding="utf-8") as f_out:
        f_out.write(write_to_conll_allennlp(docs))

    # write the likelihoods
    if doc_likelihoods is not None:
        with open(os.path.join(args.output_dir, 'likelihoods.json'), 'w', encoding="utf-8") as l_out:
            json.dump(doc_likelihoods, l_out)

    # write predicted and gold labels for all token
    with open(os.path.join(args.output_dir, 'token_gold_predicted_labels.csv'), 'w', encoding="utf-8") as l_out:
        l_out.write('token\tgold\tpredicted\n')
        for (token, g), p in zip(token_gold_labels, token_predicted_labels):
            l_out.write(f'{token}\t{g}\t{p}\n')

