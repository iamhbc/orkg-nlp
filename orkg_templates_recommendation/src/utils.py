import re
import os

from collections import Counter


def post_process(string):
    if not string:
        return string

    # replace each occurrence of one of the following characters with ' '
    characters = ['\s+-\s+', '-', '_', '\.']
    regex = '|'.join(characters)
    string = re.sub(regex, ' ', string)

    return ' '.join(string.split()).lower()


def create_sequence(sentence_1, sentence_2):
    return '[CLS] {} [SEP] {} [SEP]'.format(sentence_1, sentence_2)


def uri_to_id(uri):
    return os.path.basename(uri)


def id_to_uri(id, angle_brackets=True):
    if not angle_brackets:
        return 'http://orkg.org/orkg/resource/{}'.format(id)

    return '<http://orkg.org/orkg/resource/{}>'.format(id)


def flatten(t, key=None):
    flattened = []

    for sublist in t:
        for item in sublist:
            if key:
                flattened.append(item[key])
            else:
                flattened.append(item)

    return flattened


def count_occurrences(list):
    return dict(Counter(list))


def sanitize_abstract(abstract):
    if not abstract:
        return None

    jats_regex = '</?jats:[a-zA-Z0-9_]*>'
    abstract = re.sub(jats_regex, ' ', abstract).strip()
    return ' '.join(abstract.split())
