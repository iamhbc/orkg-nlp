import os
import json
import pandas as pd


class Writer:

    def validate_path(func):
        """
            Last argument of functions using this decorator must be a path
        """

        def wrapper(self, *args, **kwargs):
            if not os.path.exists(os.path.dirname(args[-1])):
                os.makedirs(os.path.dirname(args[-1]))
            return func(self, *args, **kwargs)

        return wrapper

    @validate_path
    def write_csv(data, columns, output_path):
        df = pd.DataFrame(data, columns=columns)
        df.to_csv(output_path, index=False)

    @validate_path
    def write_json(json_data, output_path):
        with open(output_path, 'w') as json_file:
            json.dump(json_data, json_file, indent=4)

    @validate_path
    def write_png(image, output_path):
        image.save(output_path)
