import io
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image

plt.rcParams["figure.figsize"] = (20, 10)


def plot_bert_es_measures(bert_accuracy, bert_recall, bert_precision, bert_f_measure, elasticsearch_accuracy, elasticsearch_recall, elasticsearch_precision, elasticsearch_f_measure):
    plt.clf()
    fig, ax = plt.subplots()
    width = 0.1
    offset = 0

    bert_values = [bert_accuracy, bert_recall, bert_precision, bert_f_measure]
    es_values = [elasticsearch_accuracy, elasticsearch_recall, elasticsearch_precision, elasticsearch_f_measure]
    x_labels = ['Accuracy', 'Recall', 'Precision', 'F-Measure']
    legend_labels = ['BERT', 'ES']
    index = np.arange(len(x_labels))

    for i, value in enumerate([bert_values, es_values]):
        ax.bar(index + offset, value, width=width, label=legend_labels[i])
        offset += width

    for i in range(len(bert_values)):
        # multiplying and dividing by 1000 to prevent rounding
        plt.annotate('{:.3f}'.format(int(bert_values[i] * 1000) / 1000),
                     xy=(index[i], bert_values[i]), ha='center', va='bottom')

    for i in range(len(es_values)):
        plt.annotate('{:.3f}'.format(int(es_values[i] * 1000) / 1000),
                     xy=(index[i] + width, es_values[i]), ha='center', va='bottom')

    plt.xticks(index + 0.5 * width, x_labels)

    plt.legend(loc='best')
    plt.xlabel('Measure')
    plt.ylabel('Value')

    return convert_plot_to_object(plt)


def convert_plot_to_object(plt):
    """ converts matplotlib object into a PIL image object """
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)
    im = Image.open(buf)

    return im
