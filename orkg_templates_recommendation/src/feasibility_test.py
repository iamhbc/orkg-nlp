import os

from orkg_templates_recommendation.src.dump_reader import get_by_query, create_graph
from orkg_templates_recommendation.src.io.reader import Reader
from orkg_templates_recommendation.src.io.writer import Writer
from orkg_templates_recommendation.src.static.sparql_output import ResearchFieldAndProblems
from orkg_templates_recommendation.src.static.sparql_queries import CONTRIBUTION_INFORMATION_QUERY
from orkg_templates_recommendation.src.utils import uri_to_id

FILE = os.path.dirname(os.path.realpath(__file__))
RDF_DUMP_PATH = os.path.join(FILE, '..', 'data', 'dump.nt')
OUTPUT_DIR = os.path.join(FILE, '..', 'output')
CONTRIBUTIONS_PATH = os.path.join(OUTPUT_DIR, 'contributions.json')
TEMPLATES_PATH = os.path.join(OUTPUT_DIR, 'templates.json')
MIN_OCCURRENCE_THRESHOLD = 2
MIN_OCCURRENCE_OPTIONAL_THRESHOLD = 3
MAX_CONTRIBUTIONS_THRESHOLD = 100
GENERIC_TEMPLATES = [
    'http://orkg.org/orkg/resource/R44415',
    'http://orkg.org/orkg/resource/R49174',
    'http://orkg.org/orkg/resource/R49156'
]


def get_contribution_research_field(graph, contribution):
    query_result = get_by_query(graph, CONTRIBUTION_INFORMATION_QUERY('<{}>'.format(contribution)))
    paper_research_fields_and_problems = ResearchFieldAndProblems.to_json(query_result)
    return {
        'id': paper_research_fields_and_problems['research_field'],
        'label': paper_research_fields_and_problems['research_field_label']
    }


def first_level_uses_template(template_properties, required_template_properties, contribution_properties):
    # if all template properties are optional, one property at least must be used
    if not required_template_properties:
        if len(set(template_properties).intersection(list(contribution_properties.keys()))) >= \
                min(len(template_properties), MIN_OCCURRENCE_OPTIONAL_THRESHOLD):
            return True

    # There are some required properties and they are all used
    elif set(required_template_properties).intersection(list(contribution_properties.keys())) == set(
            required_template_properties) and len(required_template_properties) >= \
            min(len(template_properties), MIN_OCCURRENCE_THRESHOLD):
        return True

    return False


def second_level_uses_template(template_properties, required_template_properties, contribution_properties):
    for contribution_property in contribution_properties:
        for object in contribution_properties[contribution_property]['objects']:
            second_level_properties = contribution_properties[contribution_property]['objects'][object]

            # if all template properties are optional, one property at least must be used
            if not required_template_properties:
                if len(set(template_properties).intersection(second_level_properties)) >= \
                        min(len(template_properties), MIN_OCCURRENCE_OPTIONAL_THRESHOLD):
                    return True

            # There are some required properties and they are all used
            elif set(required_template_properties).intersection(second_level_properties) == set(
                    required_template_properties) and len(required_template_properties) >= \
                    min(len(template_properties), MIN_OCCURRENCE_THRESHOLD):
                return True

    return False


def contribution_uses_template(contribution, template):
    template_components = template['template_components']
    template_properties = [component['property'] for component in template_components]
    required_template_properties = [component['property'] for component in template_components if
                                    component['occurrence_min'] not in ['', '0']]

    return first_level_uses_template(template_properties, required_template_properties, contribution['properties']) or \
           second_level_uses_template(template_properties, required_template_properties, contribution['properties'])


def get_using_contributions(graph, template, contributions):
    using_contributions = []

    for contribution in contributions:
        if contribution_uses_template(contributions[contribution], template):
            using_contributions.append({
                'contribution_id': contribution,
                'research_field': get_contribution_research_field(graph, contribution)
            })

    return using_contributions


def get_used_templates(graph, templates, contributions):
    used_templates = {'templates': []}

    for template in templates:
        print('Checking template {}'.format(uri_to_id(template)))

        if template in GENERIC_TEMPLATES:
            print('Skipping template {}'.format(uri_to_id(template)))
            continue

        using_contributions = get_using_contributions(graph, templates[template], contributions)

        if len(using_contributions) in range(1, MAX_CONTRIBUTIONS_THRESHOLD + 1):
            used_templates['templates'].append({
                'template_id': template,
                'research_fields': templates[template]['template_of_research_field'],
                'contributions': using_contributions
            })

    return used_templates


def main():
    graph = create_graph(RDF_DUMP_PATH)
    contributions = Reader.read_json(CONTRIBUTIONS_PATH)
    templates = Reader.read_json(TEMPLATES_PATH)

    used_templates = get_used_templates(graph, templates, contributions)
    Writer.write_json(used_templates, os.path.join(OUTPUT_DIR, 'used_templates.json'))


if __name__ == '__main__':
    main()
