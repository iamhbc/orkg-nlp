import os

from rdflib import Graph
from orkg_templates_recommendation.src.io.writer import Writer
from orkg_templates_recommendation.src.static.sparql_output import TemplatesInformation, ContributionPredicates
from orkg_templates_recommendation.src.static.sparql_queries import TEMPLATES_INFORMATION_QUERY, \
    CONTRIBUTION_PREDICATES_QUERY

FILE = os.path.dirname(os.path.realpath(__file__))
RDF_DUMP_PATH = os.path.join(FILE, '..', 'data', 'dump.nt')
OUTPUT_DIR = os.path.join(FILE, '..', 'output')


def create_graph(path):
    print('Parsing the dump ...')
    return Graph().parse(path, format='nt')


def get_by_query(graph, query):
    print('Querying ...')
    query_result = graph.query(query)

    if not query_result or len(query_result) == 0:
        return []

    result = []
    for row in query_result:
        columns = []
        for column in row:
            columns.append('' if not column else column.toPython())
        result.append(columns)

    return result


def main():
    graph = create_graph(RDF_DUMP_PATH)

    templates_information = get_by_query(graph, TEMPLATES_INFORMATION_QUERY)
    contribution_predicates = get_by_query(graph, CONTRIBUTION_PREDICATES_QUERY)

    Writer.write_json(TemplatesInformation.to_json(templates_information),
                      os.path.join(OUTPUT_DIR, 'templates.json'))
    Writer.write_json(ContributionPredicates.to_json(contribution_predicates),
                      os.path.join(OUTPUT_DIR, 'contributions.json'))


if __name__ == "__main__":
    main()
