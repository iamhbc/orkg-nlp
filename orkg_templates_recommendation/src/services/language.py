import fasttext


class LanguageDetector:
    __instance = None

    def __init__(self, model_path):
        if LanguageDetector.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            LanguageDetector.__instance = self

        self.model = self.build_model(model_path)

    @staticmethod
    def build_model(path):
        return fasttext.load_model(path)

    @staticmethod
    def get_instance(model_path):
        if LanguageDetector.__instance:
            return LanguageDetector.__instance

        return LanguageDetector(model_path)

    def detect_lang(self, text, k=1):
        """ https://github.com/facebookresearch/fastText """
        # ISO631 codes: af als am an ar arz as ast av az azb ba bar bcl be bg bh bn bo bpy br bs bxr ca cbk ce ceb
        # ckb co cs cv cy da de diq dsb dty dv el eml en eo es et eu fa fi fr frr fy ga gd gl gn gom gu gv he hi hif
        # hr hsb ht hu hy ia id ie ilo io is it ja jbo jv ka kk km kn ko krc ku kv kw ky la lb lez li lmo lo lrc lt
        # lv mai mg mhr min mk ml mn mr mrj ms mt mwl my myv mzn nah nap nds ne new nl nn no oc or os pa pam pfl pl
        # pms pnb ps pt qu rm ro ru rue sa sah sc scn sco sd sh si sk sl so sq sr su sv sw ta te tg th tk tl tr tt
        # tyv ug uk ur uz vec vep vi vls vo wa war wuu xal xmf yi yo yue zh
        text = ' '.join(text.split())
        output = self.model.predict(text, k=k)  # top k matching languages
        lang_tags, probs = output[0], output[1]

        result = []
        for lang_tag in lang_tags:
            _index = lang_tags.index(lang_tag)
            lang_tag = lang_tag.replace("__label__", "")
            result.append([lang_tag, probs[_index]])
        return result

    def is_iso(self, text, iso):
        return iso in self.detect_lang(text)[0]

    def is_english(self, text):
        return self.is_iso(text, 'en')
