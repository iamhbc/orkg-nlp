import os
from argparse import ArgumentParser

import requests

from orkg_templates_recommendation.src.io.reader import Reader
from orkg_templates_recommendation.src.io.visualizer import plot_bert_es_measures
from orkg_templates_recommendation.src.io.writer import Writer

FILE = os.path.dirname(os.path.realpath(__file__))
OUTPUT_DIR = os.path.join(FILE, '..', 'output')
TEST_SET_PATH = os.path.join(OUTPUT_DIR, 'test_set_reduced.json')
TEST_SET_ANALYZED_PATH = os.path.join(OUTPUT_DIR, 'test_set_reduced_analyzed.json')
DATASET_PATH = os.path.join(OUTPUT_DIR, 'dataset.json')
SIMCOMP_HOST = 'http://localhost:5000/similarity/template'


def parse_args():
    parser = ArgumentParser()
    parser.add_argument('-p', '--prepare', action='store_true', help='Prepare the test set before evaluating')
    parser.add_argument('-i', '--init', action='store_true', help='Init simcomp service if needed')
    return parser.parse_args()


def init_elasticsearch():
    URL = '{}/internal/init'.format(SIMCOMP_HOST)

    print('Starting init ...')
    response = requests.get(URL)

    if not response.ok:
        print(response.content)

    print('Done initing')
    print(response.json())


def similar_elasticsearch(q):
    URL = '{}/similar/?title=hello&q={}'.format(SIMCOMP_HOST, q)

    response = requests.get(URL)

    if not response.ok:
        print('Failed to get similarities')

    return response.json()


def similar_bert(q):
    URL = '{}/similar/bert/?title=hello&q={}'.format(SIMCOMP_HOST, q)

    response = requests.get(URL)

    if not response.ok:
        print('Failed to get similarities')

    return response.json()


def prepare(init=False):
    test_set = Reader.read_json(TEST_SET_PATH)

    if init:
        init_elasticsearch()

    instances = test_set['entailments'] + test_set['contradictions'] + test_set['neutrals']
    for i, instance in enumerate(instances):
        print('{}/{} - instance {}'.format(i + 1, len(instances), instance['instance_id']))

        instance['bert_results'] = similar_bert(instance['hypothesis'])
        instance['elasticsearch_results'] = similar_elasticsearch(instance['hypothesis'])

    Writer.write_json(test_set, TEST_SET_ANALYZED_PATH)


def evaluate():
    test_set = Reader.read_json(TEST_SET_ANALYZED_PATH)
    dataset = Reader.read_json(DATASET_PATH)
    number_of_templates = len(dataset['templates']) + 1  # including the neutral template

    evaluate_accuracy_precision_recall(test_set, number_of_templates)


def evaluate_accuracy_precision_recall(test_set, number_of_templates):
    """
        Accuracy = (TP + TN) / (TP + TN + FP + FN). In our case we should consider the number of contradictions
        Negative class: (number_of_templates * len(instances)) - len(instances)
        Positive class: len(instances)
        * len(entailments) controls our logic. Best cases are len(entailments) = 1 and len(entailments) = 0 for the
        entailment target and neutral target respectively.
        Recall = TP / total entailments and neutrals
        Precision = TP / TP + FP
    """
    instances = test_set['entailments'] + test_set['contradictions'] + test_set['neutrals']
    bert_accuracy = 0
    bert_tp = 0
    bert_fp = 0

    elasticsearch_accuracy = 0
    elasticsearch_tp = 0
    elasticsearch_fp = 0

    for instance in instances:
        bert_entailments = instance['bert_results'][:1]  # ignore fp
        bert_results = extract_top_k_results(bert_entailments, 1)

        elasticsearch_entailments = instance['elasticsearch_results'][:1]  # ignore fp
        elasticsearch_results = extract_top_k_results(instance['elasticsearch_results'], 1)

        if instance['target'] == 'entailment':
            # there is a hit in the response
            if instance['template_id'] in bert_results:
                bert_accuracy += number_of_templates - len(bert_entailments) + 1
                bert_tp += 1
                bert_fp += len(bert_entailments) - 1
            # there is no hit in the response
            else:
                bert_accuracy += number_of_templates - len(bert_entailments)
                bert_fp += len(bert_entailments)

        elif instance['target'] == 'neutral':
            # the number of entailments controls the accuracy of the neutrals.
            bert_accuracy += number_of_templates - len(bert_entailments)
            bert_fp += len(bert_entailments)

            if not bert_results:
                bert_tp += 1

        # ES doesn't differentiate between neutrals and entailments
        if instance['template_id'] in elasticsearch_results:
            elasticsearch_accuracy += number_of_templates - len(elasticsearch_entailments) + 1
            elasticsearch_tp += 1
            elasticsearch_fp += len(elasticsearch_entailments) - 1
        else:
            elasticsearch_accuracy += number_of_templates - len(elasticsearch_entailments)
            elasticsearch_fp += len(elasticsearch_entailments)

    # normalize
    bert_accuracy /= len(instances) * number_of_templates
    bert_recall = bert_tp / len(instances)
    bert_precision = bert_tp / (bert_tp + bert_fp)
    bert_f_measure = (2 * bert_recall * bert_precision) / (bert_recall + bert_precision)

    elasticsearch_accuracy /= len(instances) * number_of_templates
    elasticsearch_recall = elasticsearch_tp / len(instances)
    elasticsearch_precision = elasticsearch_tp / (elasticsearch_tp + elasticsearch_fp)
    elasticsearch_f_measure = (2 * elasticsearch_recall * elasticsearch_precision) / (elasticsearch_recall + elasticsearch_precision)

    print('BERT:')
    print('\taccuracy: {}'.format(bert_accuracy))
    print('\trecall: {}'.format(bert_recall))
    print('\tprecision: {}'.format(bert_precision))
    print('\tf-measure: {}'.format(bert_f_measure))

    print('ES:')
    print('\taccuracy: {}'.format(elasticsearch_accuracy))
    print('\trecall: {}'.format(elasticsearch_recall))
    print('\tprecision: {}'.format(elasticsearch_precision))
    print('\tf-measure: {}'.format(elasticsearch_f_measure))
    plot = plot_bert_es_measures(bert_accuracy, bert_recall, bert_precision, bert_f_measure, elasticsearch_accuracy, elasticsearch_recall, elasticsearch_precision, elasticsearch_f_measure)
    Writer.write_png(plot,
                     os.path.join(OUTPUT_DIR, 'bert_es_measures.png'))


def extract_top_k_results(objects, k):
    top_k = []

    for object in objects[:k]:
        top_k.append(object['template_id'])

    return top_k


def main():
    args = parse_args()

    if args.prepare:
        prepare(args.init)

    evaluate()


if __name__ == '__main__':
    main()
