from orkg_templates_recommendation.src import feasibility_test, dump_reader, dataset, split_dataset


def main():
    print('------------------------ Starting dump_reader.py ------------------------')
    dump_reader.main()

    print('------------------------ Starting feasibility_test.py ------------------------')
    feasibility_test.main()

    print('------------------------ Starting dataset.py ------------------------')
    dataset.main()

    print('------------------------ Starting split_dataset.py ------------------------')
    split_dataset.main()


if __name__ == '__main__':
    main()