class TemplatesInformation:

    @staticmethod
    def to_json(data):
        json = {}

        for row in data:
            template_id = row[0]
            template_label = row[1]
            template_component = row[2]
            template_component_property = row[3]
            template_component_property_label = row[4]
            template_component_occurrence_min = row[5]
            template_component_occurrence_max = row[6]
            template_of_class = row[7]
            template_of_class_label = row[8]
            template_of_predicate = row[9]
            template_of_predicate_label = row[10]
            template_of_research_field = row[11]
            template_of_research_field_label = row[12]
            template_of_research_problem = row[13]
            template_of_research_problem_label = row[14]
            template_label_format = row[15]
            template_strict = row[16]

            if template_id not in json:
                json[template_id] = {}
                json[template_id]['template_label'] = template_label
                json[template_id]['template_components'] = []
                json[template_id]['template_of_class'] = []
                json[template_id]['template_of_predicate'] = []
                json[template_id]['template_of_research_field'] = []
                json[template_id]['template_of_research_problem'] = []
                json[template_id]['template_label_format'] = template_label_format
                json[template_id]['template_strict'] = template_strict

            template_component_object = {
                'id': template_component,
                'property': template_component_property,
                'property_label': template_component_property_label,
                'occurrence_min': template_component_occurrence_min,
                'occurrence_max': template_component_occurrence_max
            }

            if template_component_object not in json[template_id]['template_components']:
                json[template_id]['template_components'].append(template_component_object)

            template_of_class_object = {
                'id': template_of_class,
                'label': template_of_class_label
            }

            if template_of_class_object not in json[template_id]['template_of_class']:
                json[template_id]['template_of_class'].append(template_of_class_object)

            template_of_predicate_object = {
                'id': template_of_predicate,
                'label': template_of_predicate_label
            }

            if template_of_predicate_object not in json[template_id]['template_of_predicate']:
                json[template_id]['template_of_predicate'].append(template_of_predicate_object)

            template_of_research_field_object = {
                'id': template_of_research_field,
                'label': template_of_research_field_label
            }

            if template_of_research_field_object not in json[template_id]['template_of_research_field']:
                json[template_id]['template_of_research_field'].append(template_of_research_field_object)

            template_of_research_problem_object = {
                'id': template_of_research_problem,
                'label': template_of_research_problem_label
            }

            if template_of_research_problem_object not in json[template_id]['template_of_research_problem']:
                json[template_id]['template_of_research_problem'].append(template_of_research_problem_object)

        return json


class ContributionPredicates:

    @staticmethod
    def to_json(data):
        json = {}

        BLACK_LIST = [
            'http://www.w3.org/2000/01/rdf-schema#label',
            'http://www.w3.org/1999/02/22-rdf-syntax-ns#type'
        ]

        for row in data:
            contribution_id = row[0]

            if row[1] in BLACK_LIST:
                continue

            if contribution_id not in json:
                json[contribution_id] = {}
                json[contribution_id]['properties'] = {}

            if row[1] not in json[contribution_id]['properties']:
                json[contribution_id]['properties'][row[1]] = {}
                json[contribution_id]['properties'][row[1]]['objects'] = {}

            if row[2] not in json[contribution_id]['properties'][row[1]]['objects']:
                json[contribution_id]['properties'][row[1]]['objects'][row[2]] = []

            if row[3] and row[3] not in BLACK_LIST:
                json[contribution_id]['properties'][row[1]]['objects'][row[2]].append(row[3])

        return json


class ResearchFieldAndProblems:

    @staticmethod
    def to_json(data):
        json = {'research_field': '', 'research_field_label': '', 'research_problems': []}

        for row in data:
            json['research_field'] = row[0]
            json['research_field_label'] = row[1]
            json['research_problems'].append(row[2])

        return json
