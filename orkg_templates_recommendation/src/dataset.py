import os
import random

from orkg_templates_recommendation.src.dump_reader import get_by_query, create_graph
from orkg_templates_recommendation.src.io.reader import Reader
from orkg_templates_recommendation.src.io.writer import Writer
from orkg_templates_recommendation.src.services.language import LanguageDetector
from orkg_templates_recommendation.src.services.metadata import MetadataService
from orkg_templates_recommendation.src.static.sparql_queries import CONTRIBUTION_PAPER, PAPERS_BY_RESEARCH_FIELD_QUERY, \
    PAPERS_QUERY
from orkg_templates_recommendation.src.utils import uri_to_id, id_to_uri

FILE = os.path.dirname(os.path.realpath(__file__))
RDF_DUMP_PATH = os.path.join(FILE, '..', 'data', 'dump.nt')
LANGUAGE_DETECTOR_PATH = os.path.join(FILE, '..', 'models', 'lang_model.ftz')
OUTPUT_DIR = os.path.join(FILE, '..', 'output')
TEMPLATES_INFORMATION_PATH = os.path.join(OUTPUT_DIR, 'templates.json')
USED_TEMPLATES_PATH = os.path.join(OUTPUT_DIR, 'used_templates.json')
MIN_PAPERS_THRESHOLD = 2
random.seed(10)


def fetch_papers_information(used_template, graph):
    metadata_service = MetadataService.get_instance()
    language_detector = LanguageDetector.get_instance(LANGUAGE_DETECTOR_PATH)
    papers = []
    paper_ids = []

    for contribution in used_template['contributions']:
        paper_info = get_by_query(graph, CONTRIBUTION_PAPER('<{}>'.format(contribution['contribution_id'])))

        if paper_info:
            paper_info = paper_info[0]

        # paper already fetched
        if not paper_info or paper_info[0] in paper_ids:
            continue

        # only consider english papers
        if not language_detector.is_english(paper_info[1]):
            continue

        paper = {
            'id': uri_to_id(paper_info[0]),
            'label': paper_info[1],
            'doi': paper_info[2],
            'research_field': {
                'id': uri_to_id(paper_info[3]),
                'label': paper_info[4]
            },
            'abstract': metadata_service.by_doi(paper_info[2]) or metadata_service.by_title(paper_info[1])
        }

        # ignore papers without abstracts
        if not paper['abstract']:
            continue

        papers.append(paper)
        paper_ids.append(paper_info[0])

    return papers, paper_ids


def construct_dataset(templates_information, used_templates, graph):
    dataset = {'templates': []}
    dataset_paper_ids = []

    for used_template in used_templates['templates']:
        papers, paper_ids = fetch_papers_information(used_template, graph)

        # We need min MIN_PAPERS_THRESHOLD papers to be able to split the test/training sets.
        if len(papers) < MIN_PAPERS_THRESHOLD:
            continue

        dataset['templates'].append({
            'id': uri_to_id(used_template['template_id']),
            'label': templates_information[used_template['template_id']]['template_label'],
            'research_fields': used_template['research_fields'],
            'properties': [component['property_label'] for component in
                           templates_information[used_template['template_id']]['template_components']],
            'papers': papers
        })
        dataset_paper_ids.extend(paper_ids)

    return dataset, dataset_paper_ids


def fetch_neutral_papers_with_same_distribution(paper_ids, research_fields, graph):
    metadata_service = MetadataService.get_instance()
    language_detector = LanguageDetector.get_instance(LANGUAGE_DETECTOR_PATH)
    neutral_papers = []
    neutral_paper_ids = []

    for research_field_id in list(research_fields.keys()):
        print('Fetching neutral papers for research field {}'.format(research_field_id))
        orkg_papers = get_by_query(graph, PAPERS_BY_RESEARCH_FIELD_QUERY(id_to_uri(research_field_id, angle_brackets=True)))

        temp_neutral_papers = []
        temp_neutral_paper_ids = []

        for orkg_paper in orkg_papers:

            # stop if we got what we need
            if len(temp_neutral_papers) == len(research_fields[research_field_id]['papers']):
                break

            # papers don't use templates and have not been seen before
            # only consider english papers
            if orkg_paper[0] in paper_ids + neutral_paper_ids + temp_neutral_paper_ids\
                    or not language_detector.is_english(orkg_paper[1]):
                continue

            temp_neutral_paper = {
                'id': uri_to_id(orkg_paper[0]),
                'label': orkg_paper[1],
                'doi': orkg_paper[2],
                'research_field': {
                    'id': research_field_id,
                    'label': research_fields[research_field_id]['label']
                },
                'abstract': metadata_service.by_doi(orkg_paper[2]) or metadata_service.by_title(orkg_paper[1])
            }

            # ignore papers without abstracts
            if not temp_neutral_paper['abstract']:
                continue

            temp_neutral_papers.append(temp_neutral_paper)
            temp_neutral_paper_ids.append(orkg_paper[0])

        neutral_papers.extend(temp_neutral_papers)
        neutral_paper_ids.extend(temp_neutral_paper_ids)

    return neutral_papers, neutral_paper_ids


def fetch_rest_neutral_papers(paper_ids, neutral_paper_ids, graph):
    metadata_service = MetadataService.get_instance()
    language_detector = LanguageDetector.get_instance(LANGUAGE_DETECTOR_PATH)
    rest_neutral_papers = []
    rest_neutral_paper_ids = []

    orkg_papers = get_by_query(graph, PAPERS_QUERY)

    while len(rest_neutral_papers) + len(neutral_paper_ids) < len(paper_ids):
        orkg_paper = random.choice(orkg_papers)

        # papers don't use templates and have not been seen before
        if orkg_paper[0] in paper_ids + neutral_paper_ids + rest_neutral_paper_ids:
            continue

        # only consider english papers
        if not language_detector.is_english(orkg_paper[1]):
            continue

        neutral_paper = {
            'id': uri_to_id(orkg_paper[0]),
            'label': orkg_paper[1],
            'doi': orkg_paper[2],
            'research_field': {
                'id': uri_to_id(orkg_paper[3]),
                'label': orkg_paper[4]
            },
            'abstract': metadata_service.by_doi(orkg_paper[2]) or metadata_service.by_title(orkg_paper[1])
        }

        # ignore papers without abstracts
        if not neutral_paper['abstract']:
            continue

        print('{}/{}'.format(len(rest_neutral_papers) + len(neutral_paper_ids) + 1, len(paper_ids)))
        rest_neutral_papers.append(neutral_paper)
        rest_neutral_paper_ids.append(orkg_paper[0])

    return rest_neutral_papers, rest_neutral_paper_ids


def extract_papers_research_fields(dataset):
    research_fields = {}

    for template in dataset['templates']:
        for paper in template['papers']:

            if paper['research_field']['id'] not in research_fields:
                research_fields[paper['research_field']['id']] = {'label': '', 'papers': []}

            research_fields[paper['research_field']['id']]['label'] = paper['research_field']['label']
            research_fields[paper['research_field']['id']]['papers'].append(
                paper['id'])

    dataset['papers_research_fields'] = research_fields
    return dataset


def main():
    templates_information = Reader.read_json(TEMPLATES_INFORMATION_PATH)
    used_templates = Reader.read_json(USED_TEMPLATES_PATH)
    graph = create_graph(RDF_DUMP_PATH)

    # entailments / papers using templates.
    dataset, dataset_paper_ids = construct_dataset(templates_information, used_templates, graph)
    research_fields = extract_papers_research_fields(dataset)['papers_research_fields']

    Writer.write_json(dataset, os.path.join(OUTPUT_DIR, 'dataset.json'))

    # papers that do not use any template and have the same distribution as the previous papers
    neutral_papers, neutral_paper_ids = fetch_neutral_papers_with_same_distribution(dataset_paper_ids, research_fields, graph)
    dataset['neutral_papers'] = neutral_papers

    # papers that do not use any template and have a different distribution as the previous papers
    rest_neutral_papers, rest_neutral_paper_ids = fetch_rest_neutral_papers(dataset_paper_ids, neutral_paper_ids, graph)
    dataset['neutral_papers'].extend(rest_neutral_papers)

    del dataset['papers_research_fields']
    Writer.write_json(dataset, os.path.join(OUTPUT_DIR, 'dataset.json'))


if __name__ == '__main__':
    main()
